import React, { Component } from 'react';

import { CardList } from './components/card-list/card-list.component';
import { SearchBox } from './components/search-box/search-box.component';

import './App.css';

class App extends Component{
  constructor(){
    super();

    this.state = {
      monsters: [],
      searchField: ''
    }

  }

  // life cycle methods: when this components mount/ React renders our component to the DOM for the first time; it will call whatever block of code within the componentDidMount method
  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(users => this.setState({monsters: users}))
  }

  // lexical scoping of arrow function
  handleChange = (e) => {
    this.setState({searchField: e.target.value});
  }

  render(){

    // destructuring
    const { monsters, searchField } = this.state;
    const filteredMonsters = monsters.filter(monster => 
      monster.name.toLowerCase().includes(searchField.toLowerCase())
      )

    return (
      <div className="App">
        
        <h1>Monsters Rolodex</h1>

        <SearchBox
        placeholder='search monsters'
        handleChange={this.handleChange}
        />

        <CardList monsters={filteredMonsters} />
        
      </div>
    );
  }
}


export default App;
