<h1 align="center">Welcome to Monsters Rolodex 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.1.0-blue.svg?cacheSeconds=2592000" />
  <a href="LICENSE" target="_blank">
    <img alt="License: MIT License" src="https://img.shields.io/badge/License-MIT License-yellow.svg" />
  </a>
  <a href="https://twitter.com/collinsmuriuki_" target="_blank">
    <img alt="Twitter: collinsmuriuki_" src="https://img.shields.io/twitter/follow/collinsmuriuki_.svg?style=social" />
  </a>
</p>

> A simple React app which covers the basic fundamentals of React; state, class and functional components, handling events, component life cycle methods and handling asynchronous tasks using promises.

### 🏠 [Homepage](https://collinsmuriuki.github.io/monsters-rolodex)

## Install

```sh
yarn
```

## Usage

```sh
yarn start
```

## Author

👤 **Collins Muriuki**

* Website: https://muriuki.dev
* Twitter: [@collinsmuriuki_](https://twitter.com/collinsmuriuki_)
* Github: [@collinsmuriuki](https://github.com/collinsmuriuki)
* LinkedIn: [@collinsmuriuki](https://linkedin.com/in/collinsmuriuki)

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

Copyright © 2019 [Collins Muriuki](https://github.com/collinsmuriuki).<br />
This project is [MIT License](LICENSE) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_